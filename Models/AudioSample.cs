﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudioApi.Models
{
    public class AudioSample
    {
        public string FileName { get; set; }
        public double Duration { get; set; }
        public byte[] Data { get; set; }
    }
}
