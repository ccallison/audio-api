﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudioApi.Models
{
    public class AudioFileMetadata
    {
        public string FileName { get; set; }
        public string FullPath { get; set; }
        public double Duration { get; set; }
        public long FileSize { get; set; }
    }
}
