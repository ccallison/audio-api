﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudioApi.Models
{
    public class AudioSampleBatch
    {
        public int BatchId { get; set; }
        public DateTime CreateTime { get; set; }
        public IEnumerable<AudioSample> Samples { get; set; }
    }
}
