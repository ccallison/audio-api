﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using AudioApi.Models;
using AudioApi.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AudioApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AudioController : ControllerBase
    {
        private IAudioFileManagerService _fileManager;

        public AudioController(IAudioFileManagerService fileManager)
        {
            _fileManager = fileManager;
        }

        // GET: api/Audio/list
        [HttpGet("list")]
        public IEnumerable<string> Get([FromQuery(Name = "maxduration")] int? maxDuration)
        {
            return _fileManager.ListAudioFiles(maxDuration);
        }

        // GET: api/Audio/info
        [HttpGet("info")]
        public AudioFileMetadata Get([FromQuery(Name = "name")] string fileName)
        {
            return _fileManager.GetAudioFileMetadata(fileName);
        }

        // GET: api/Audio/info
        [HttpGet("download")]
        public IActionResult Download([FromQuery(Name = "name")] string fileName)
        {
            Stream stream = _fileManager.GetAudioFile(fileName);

            if (stream == null)
                return NotFound(); // returns a NotFoundResult with Status404NotFound response.

            return File(stream, "application/octet-stream", fileName); // returns a FileStreamResult
        }

        // POST: api/Audio
        [HttpPost, DisableRequestSizeLimit]
        public IActionResult Post()
        {
            try
            {
                var file = Request.Form.Files[0];
                _fileManager.StoreAudioFile(file);
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error");
            }

            return Ok();
        }
    }
}
