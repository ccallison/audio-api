﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AudioApi.Models;
using AudioApi.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AudioApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FragmentationController : ControllerBase
    {
        private IAudioFileManagerService _fileManager;
        private IFragmentationService _fragService;

        public FragmentationController(IAudioFileManagerService fileManager, IFragmentationService fragService)
        {
            _fileManager = fileManager;
            _fragService = fragService;
        }

        [HttpGet("batch")]
        public AudioSampleBatch Get([FromQuery(Name = "maxlength")] int maxSampleLength = 10)
        {
            var fileNames = _fileManager.ListAudioFiles(null);

            return _fragService.GetSampleBatch(fileNames, maxSampleLength);
        }
    }
}