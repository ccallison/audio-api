﻿using AudioApi.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace AudioApi.Services.Interfaces
{
    public interface IAudioFileManagerService
    {
        public void StoreAudioFile(IFormFile file);
        public IEnumerable<string> ListAudioFiles(int? maxDuration);
        public Stream GetAudioFile(string fileName);
        public AudioFileMetadata GetAudioFileMetadata(string fileName);
        string SanitizeFileLocation(string fileName);
    }
}
