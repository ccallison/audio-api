﻿using AudioApi.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace AudioApi.Services.Interfaces
{
    public interface IFragmentationService
    {
        public AudioSampleBatch GetSampleBatch(IEnumerable<string> fileNames, int maxSampleLngth);
        public AudioSample GetRandomSampleFromFile(string fileName, int maxSampleLngth);
    }
}
