﻿using AudioApi.Extensions;
using AudioApi.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using AudioApi.Utilities;
using AudioApi.Models;

namespace AudioApi.Services
{
    public class FragmentationService : IFragmentationService
    {
        IAudioFileManagerService _fileManager;

        public FragmentationService(IAudioFileManagerService fileManager)
        {
            _fileManager = fileManager;
        }

        public AudioSampleBatch GetSampleBatch(IEnumerable<string> fileNames, int maxSampleLngth)
        {
            var batch = new AudioSampleBatch
            {
                BatchId = new Random().Next(),
                CreateTime = DateTime.Now,
                Samples = fileNames.Select(fn => GetRandomSampleFromFile(fn, maxSampleLngth))
            };

            return batch;
        }

        public AudioSample GetRandomSampleFromFile(string fileName, int maxSampleLngth)
        {
            var filePath = _fileManager.SanitizeFileLocation(fileName);
            var metadata = _fileManager.GetAudioFileMetadata(filePath);

            var randGenerator = new Random();
            var randStart = randGenerator.Next(0, (int)metadata.Duration / 2);
            var randEnd = randGenerator.Next((int)metadata.Duration / 2, (int)metadata.Duration);

            //Maximum length of sample
            if(randEnd - randStart > maxSampleLngth)
            {
                randEnd = randStart + maxSampleLngth;
            }

            var trimmedFilePath = _fileManager.SanitizeFileLocation($"{Path.GetFileNameWithoutExtension(filePath)}-trimmed-{randStart}-{randEnd}.wav");

            WavFileUtils.TrimWavFile(
                filePath,
                trimmedFilePath,
                TimeSpan.FromSeconds(randStart),
                TimeSpan.FromSeconds(randEnd)
            );

            return new AudioSample
            {
                FileName = trimmedFilePath,
                Duration = _fileManager.GetAudioFileMetadata(trimmedFilePath).Duration,
                Data = _fileManager.GetAudioFile(trimmedFilePath).ToArray()
            };           
        }
    }
}
