﻿using AudioApi.Models;
using AudioApi.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using NAudio.Wave;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;

namespace AudioApi.Services
{
    public class LocalAudioFileManagerService : IAudioFileManagerService
    {

        private string _uploadsDirectory;

        public LocalAudioFileManagerService()
        {
            var folderName = Path.Combine("Uploads", "Audio");
            _uploadsDirectory = Path.Combine(Directory.GetCurrentDirectory(), folderName);
        }

        public void StoreAudioFile(IFormFile file)
        {
            if (file.Length > 0)
            {
                var fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                var fullPath = Path.Combine(_uploadsDirectory, fileName);
                var dbPath = Path.Combine(_uploadsDirectory, fileName);

                using (var stream = new FileStream(fullPath, FileMode.Create))
                {
                    file.CopyTo(stream);
                }

                return;
            }
            else
            {
                throw new Exception("File length is zero");
            }
        }

        public IEnumerable<string> ListAudioFiles(int? maxDuration)
        {
            var files = Directory.GetFiles(_uploadsDirectory);
            if (maxDuration == null)
            {
                return files.Select(f => Path.GetFileName(f));
            }
            else
            {
                return files
                    .Where(f => GetAudioFileMetadata(f).Duration <= maxDuration)
                    .Select(f => Path.GetFileName(f));
            }
        }

        public Stream GetAudioFile(string fileName)
        {
            return File.OpenRead(SanitizeFileLocation(fileName));
        }

        public AudioFileMetadata GetAudioFileMetadata(string fileName)
        {
            var fullPath = SanitizeFileLocation(fileName);

            if (File.Exists(fullPath))
            {
                var fileInfo = new FileInfo(fullPath);
                var audioInfo = new WaveFileReader(fullPath);

                return new AudioFileMetadata
                {
                    FileName = fileInfo.Name,
                    FullPath = fileInfo.FullName,
                    FileSize = fileInfo.Length,
                    Duration = audioInfo.TotalTime.TotalSeconds
                };
            }
            else
            {
                throw new FileNotFoundException($"Specified file does not exist: {fileName}");
            }
            
        }

        public string SanitizeFileLocation(string fileName)
        {
            if (Path.IsPathRooted(fileName))
            {
                return fileName;
            }
            else
            {
                return Path.Combine(_uploadsDirectory, fileName);
            }
        }
    }
}
